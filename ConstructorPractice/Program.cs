﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructorPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            //TODO: Create four instances of the 'Human' class, each using a different constructor

            //TODO: Have each instantiation call the 'PrintDetails method

            //TODO: If doing the bonus, create an instance of the human class using that constructor, then print its details (just like the others)

            Console.ReadKey();

        }
    }
}
