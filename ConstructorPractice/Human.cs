﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructorPractice
{
    class Human
    {
        //-------------------- PART I: DECLARE SIX FEILD VARIABLES --------------------\\

        //TODO: Declare two field variables of type double - one named '_height', the other '_weight'

        //TODO: Declare an integer variable named '_age'

        //TODO: Declare three string vaiables, named '_name', '_gender', and '_homeTown'

        //-------------------- PART II: CREATE FIRST CONSTRUCTOR --------------------\\

        //TODO: Create constructor that takes no parameters
        //TODO: Initialize all of the field variables to be any value you want

        //-------------------- PART III: CREATE SECOND CONSTRUCTOR --------------------\\

        //TODO: Create constructor that takes two parameters, one for age, the other for name
        //TODO: Set the age and name variables to be the parameters,
        //TODO: Set the gender to be "gender unknown" and the hometown to be any place you want

        //-------------------- PART IV: CREATE THIRD CONSTRUCTOR --------------------\\

        //TODO: Create constructor that takes three parameters, one for height, one for weight, and the last for hometown
        //TODO: Set the height and weight variables to be the parameters, set the name to "somebody"
        //TODO: set the gender to "gender unknown", set the hometown variable to the hometown parameter

        //-------------------- PART V: CREATE FOURTH CONSTRUCTOR --------------------\\

        //TODO: Create constructor that takes all six parameters (one for each field variable)
        //TODO: Set the value of each field variable to be its respective parameter

        //-------------------- BONUS: CREATE OVERLOADING CONSTRUCTOR --------------------\\

        //TODO: Create constructor that takes the gender as the parameter, then have it overload and call Constructor 3
        //TODO: Set the name to "Bonus Bob" or "Bonus Beth", and the gender to be the gender parameter

        //HINT: Should only be two lines of code within the constructor


        /// <summary>
        ///  Method used to print the details of the human that is created (ALREADY CREATED FOR YOU)
        /// </summary>
        public void PrintDetails()
        {
            //*DO NOT EDIT THIS
            Console.WriteLine($"{_name} is a {_gender} who is {_age} years old from {_homeTown}. They are {_height} cm tall and they weigh {_weight} kg.");
        }
    }
}
